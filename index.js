const FRAMES_PER_SECOND = 60
const FRAME_WAIT_TIME = 1000 / FRAMES_PER_SECOND

const $force = document.querySelector('#force')
const $touches = document.querySelector('#touches')

const btnPlay = document.querySelector('button#play')
const btnSave = document.querySelector('button#save')
const btnClear = document.querySelector('button#clear')

const requestIdleCallback = window.requestIdleCallback || function (fn) { setTimeout(fn, 1) };

const canvas = document.querySelector('canvas')
const context = canvas.getContext('2d')

const { Videomatic, VideomaticEvent } = window.videomatic

const videomatic = new Videomatic()

let lineWidth = 0
let isMousedown = false
let points = []

context.strokeStyle = 'black'
context.lineCap = 'round'
context.lineJoin = 'round'

btnPlay.addEventListener('click', handlePlayClick)
btnSave.addEventListener('click', handleSaveClick)
btnClear.addEventListener('click', handleClearClick)

canvas.width = window.innerWidth * 2
canvas.height = window.innerHeight * 2

videomatic.on(VideomaticEvent.EXIT, handleVideomaticExitEvent)
videomatic.on(VideomaticEvent.PLAY, handleVideomaticPlayEvent)

for (const ev of ["touchstart", "mousedown"]) {
  canvas.addEventListener(ev, handleStartEvent)
}

for (const ev of ['touchmove', 'mousemove']) {
  canvas.addEventListener(ev, handleMoveEvent)
}

for (const ev of ['touchend', 'touchleave', 'mouseup']) {
  canvas.addEventListener(ev, handleStopEvent)
}

function handleStartEvent(e) {
  let pressure = 0.1;
  let x, y;
  if (e.touches && e.touches[0] && typeof e.touches[0]["force"] !== "undefined") {
    if (e.touches[0]["force"] > 0) {
      pressure = e.touches[0]["force"]
    }
    x = e.touches[0].pageX * 2
    y = e.touches[0].pageY * 2
  } else {
    pressure = 1.0
    x = e.pageX * 2
    y = e.pageY * 2
  }

  isMousedown = true

  lineWidth = Math.log(pressure + 1) * 40
  context.lineWidth = lineWidth// pressure * 50;
  // context.strokeStyle = 'black'
  // context.lineCap = 'round'
  // context.lineJoin = 'round'

  const point = { x, y, lineWidth, ts: Date.now(), started: true }
  points.push(point)
  startDrawOnCanvas(point)
}

function handleMoveEvent(e) {
  if (!isMousedown) return
  e.preventDefault()

  let pressure = 0.1
  let x, y
  if (e.touches && e.touches[0] && typeof e.touches[0]["force"] !== "undefined") {
    if (e.touches[0]["force"] > 0) {
      pressure = e.touches[0]["force"]
    }
    x = e.touches[0].pageX * 2
    y = e.touches[0].pageY * 2
  } else {
    pressure = 1.0
    x = e.pageX * 2
    y = e.pageY * 2
  }

  // smoothen line width
  lineWidth = (Math.log(pressure + 1) * 40 * 0.2 + lineWidth * 0.8)
  points.push({ x, y, lineWidth, ts: Date.now(), moving: true })

  drawOnCanvas()

  const touch = e.touches ? e.touches[0] : null

  if (touch) {
    drawTouchPressure(touch, pressure)
  }
}

function handleStopEvent(e) {
  let pressure = 0.1;
  let x, y;

  if (e.touches && e.touches[0] && typeof e.touches[0]["force"] !== "undefined") {
    if (e.touches[0]["force"] > 0) {
      pressure = e.touches[0]["force"]
    }
    x = e.touches[0].pageX * 2
    y = e.touches[0].pageY * 2
  } else {
    pressure = 1.0
    x = e.pageX * 2
    y = e.pageY * 2
  }

  isMousedown = false

  const point = { x, y, lineWidth, ts: Date.now(), stopped: true }
  points.push(point)

  stopDrawOnCanvas(point)
}

function handlePlayClick() {
  clearCanvas()
  drawPoints(0)
}

function handleClearClick() {
  points = []
  clearCanvas()
}

function clearCanvas() {
  context.clearRect(0, 0, canvas.width, canvas.height)
}

function drawPoints(pointsDrawnCount, lastDrawTime) {
  console.log('drawPoints', { pointsDrawnCount })

  if (points.length > pointsDrawnCount) {
    requestAnimationFrame(function(time) {
      console.log({ requestAnimationFrameTime: time })

      const point = points[pointsDrawnCount]

      // this would preserve the real time delay when drawing
      // if (time - lastDrawTime >= lastPoint.ts) {
        if (point.started) {
          startDrawOnCanvas(point)
        } else if (point.stopped) {
          stopDrawOnCanvas(point)
        } else {
          drawOnCanvas(pointsDrawnCount)
        }
        pointsDrawnCount++
      // }
      if (pointsDrawnCount < points.length) {
        drawPoints(pointsDrawnCount, Date.now())
      }
    })
  }
}

function drawOnCanvas(lastPointIndex) {
  context.strokeStyle = 'black'
  context.lineCap = 'round'
  context.lineJoin = 'round'

  // if (points.length >= 3) {
    const i = lastPointIndex || points.length - 1
    const xc = (points[i].x + points[i - 1].x) / 2
    const yc = (points[i].y + points[i - 1].y) / 2
    context.lineWidth = points[i - 1].lineWidth
    context.quadraticCurveTo(points[i - 1].x, points[i - 1].y, xc, yc)
    context.stroke()
    // context.beginPath()
    context.moveTo(xc, yc)

    console.log({ point: points[i], diff: points[i].ts - points[i - 1].ts })
  // }
}

function startDrawOnCanvas(point) {
  const { x, y } = point

  context.beginPath()
  context.moveTo(x, y)
}

function stopDrawOnCanvas(point) {
  context.strokeStyle = 'black'
  context.lineCap = 'round'
  context.lineJoin = 'round'

  // if (points.length >= 3) {
    const index = points.indexOf(point) - 1
    const prevPoint = points[index]
    // debugger
    context.quadraticCurveTo(prevPoint.x, prevPoint.y, point.x, point.y)
    context.stroke()
    // context.closePath()
  // }

  lineWidth = 0
}

function drawTouchPressure(touch, pressure) {
  requestIdleCallback(() => {
    $force.textContent = 'force = ' + pressure
    $touches.innerHTML = `
      touchType = ${touch.touchType} ${touch.touchType === 'direct' ? '👆' : '✍️'} <br/>
      radiusX = ${touch.radiusX} <br/>
      radiusY = ${touch.radiusY} <br/>
      rotationAngle = ${touch.rotationAngle} <br/>
      altitudeAngle = ${touch.altitudeAngle} <br/>
      azimuthAngle = ${touch.azimuthAngle} <br/>
    `
  })
}

function handleSaveClick() {
  videomatic.save(points)
}

function handleVideomaticPlayEvent(data) {
  clearCanvas()
  points = data
  drawPoints(0)
}

function handleVideomaticExitEvent() {
  console.log('videomatic exit')
}
